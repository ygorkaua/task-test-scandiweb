<?php
include '../includes/autoloader.php';

use Model\Collection;

$collection = new Collection();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - Task Test Scandiweb</title>
</head>
<body>
    <?= $collection->deleteById(1); ?>
</body>
</html>