<?php

spl_autoload_register('autoloader');

function autoloader($className) {
    $path = "../src/";
    $extension = ".php";
    $fullPath = $path . $className . $extension;

    if (file_exists($fullPath)) {
        require_once $fullPath;
    }
}
