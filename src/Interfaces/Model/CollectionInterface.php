<?php

namespace Interfaces\Model;

use PDOStatement;

interface CollectionInterface
{
    const PRODUCT_TABLE = 'products';

    /**
     * Get product by id
     *
     * @return false|PDOStatement
     */
    public function getById();

    /**
     * Delete product by id
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}