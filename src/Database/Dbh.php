<?php

namespace Database;

use PDO;

/**
 * Class to connect to database
 */
class Dbh
{
    /**
     * @var string
     */
    private string $host = 'localhost';

    /**
     * @var string
     */
    private string $user = 'admin';

    /**
     * @var string
     */
    private string $password = 'admin';

    /**
     * @var string
     */
    private string $dbName = 'scandiweb';

    /**
     * Return a PDO database connection
     *
     * @return PDO
     */
    protected function connect(): PDO
    {
        $dns = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($dns, $this->user, $this->password);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        return $pdo;
    }
}