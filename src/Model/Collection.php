<?php

namespace Model;

use PDO;
use Database\Dbh;
use Interfaces\Model\CollectionInterface;

/**
 * Class to interact with database
 */
class Collection extends Dbh implements CollectionInterface
{
    /**
     * @var PDO
     */
    private PDO $db;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->db = $this->connect();
    }

    /**
     * {@inheritDoc}
     */
    public function getById()
    {
        $sql = 'SELECT * FROM ' . self::PRODUCT_TABLE . ' WHERE "id" = ' . $_POST['id'];
        return $this->db->query($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteById(int $id): bool
    {
        $sql = 'DELETE * FROM ' . self::PRODUCT_TABLE . ' WHERE "id" = ' . $id;
        return $this->db->query($sql);
    }
}